<?php

    /**
     * Collects, determines and executes the correct route
     *
     * Parses routes' patterns and instantiates the matching callback
     *
     * @category   Nifty
     * @package    Router
     * @author     Piers Simms <piers@c1h.co.uk>
     * @copyright  2014 Piers Simms
     * @version    1.0
     * @since      1.0
     */

    namespace Nifty\Router;

    use Nifty\Router\Collections\AbstractRouteCollection;
    use Nifty\Router\AbstractRoute;
    use Nifty\Router\RouteCallbackInterface;

    /**
     * RouterService class.
     */
    class RouterService {

        /**
         * Collection of route objects
         *
         * @var mixed
         * @access protected
         */
        protected $routeCollection;

        /**
         * Chosen route
         *
         * @var mixed
         * @access protected
         */
        protected $whichRoute;

        /**
         * Arguments to be passed into matching route's callback constructor
         *
         * (default value: array())
         *
         * @var array
         * @access protected
         */
        protected $arguments = array();

        protected $method;

        protected $uri;

        /**
         * Gathers properties.
         *
         * @access public
         * @return void
         */
        public function __construct(){
            $args = func_get_args();
            if(!isset($args[0]) || !$args[0] instanceof AbstractRouteCollection){
                throw new RouterServiceException("Route collection argument is not a route collection.");
            } else {
                $this->routeCollection = $args[0];
            }
            if(!isset($args[1]) || !$args[1]){
                throw new RouterServiceException("URI argument not given.");
            } else {
                $this->uri = $args[1];
            }
            if(!isset($args[2]) || !$args[2]){
                throw new RouterServiceException("Method argument not given.");
            } else {
                $this->method = strtoupper($args[2]);
            }
            $this->arguments = array_slice(func_get_args(), 3);
        }

        /**
         * Parses $route's matching pattern's parameters and returns whether the current URI matches that pattern.
         *
         * Based extensively on (basically an exact copy of) the wonderful Slim Framework's Router (http://www.slimframework.com/) released under the MIT Public License (http://www.slimframework.com/license)
         * Slim Framework is copyright of Josh Lockhart (https://github.com/codeguy)
         *
         * @access private
         * @param AbstractRoute $route
         * @return bool
         */
        private function parseRouteAndMatchURI(AbstractRoute $route){
            $pattern = $route->getPattern();
            $uri = strtok($this->uri, '?'); // not concerned with GET parameters
            $uri = preg_replace('#/{2,}#', '/', $uri); // replace multiple slashes with just one
            $matchNames = array();
            $matchNamesEndingInPlus = array();

            $patternAsRegex = '#^'; // start regex
            $patternAsRegex .= ($pattern[0] !== '/') ? '/?' : ''; // make starting slashes optional
            // will match 1 or more word character surrounded by angle brackets with an optional, literal + on the end
            $patternAsRegex .= preg_replace_callback('#<([\w]{1,})>\+?#', function($match) use(&$matchNames, &$matchNamesEndingInPlus, $route){ // anon function

                    $matchRegex = $match[0]; // <matchName>+ with brackets
                    $matchNames[] = $matchName = $match[1]; // matchName without brackets

                    $conditions = $route->getConditions();
                    if(isset($conditions[$matchName])){ // route's conditions are a static property in controller
                        return '(?P' . $matchRegex . $conditions[$matchName] . ')'; // could be any regex
                    } elseif(substr($matchRegex, -1) === '+'){
                        $matchNamesEndingInPlus[$matchName] = true; // $matchNamesEndingInPlus has been imported by reference
                        // $matchRegex without the + at the end
                        return '(?P' . substr($matchRegex, 0, strlen($matchRegex)-1) . '[^\n]{1,})'; // one or more characters that are not newlines
                    } else {
                        // (?P<matchName>[charClass]) names the character class match
                        return '(?P' . $matchRegex . '[^/]{1,})'; // one or more characters that are not slashes
                    }

                }, $pattern);
            $patternAsRegex .= (substr($pattern, -1) === '/') ? '?' : ''; // ending slashes are optional
            $patternAsRegex .= '$#i'; // end regex and is case-insensitive

            if(!preg_match($patternAsRegex, $uri, $matches)){
                return false;
            } else {
                foreach($matchNames as $matchName){ // foreach parameter in the pattern
                    if(isset($matches[$matchName])){ // if we found a match for it
                        if(isset($matchNamesEndingInPlus[$matchName])){
                            $matches[$matchName] = explode('/', $matches[$matchName]); // split directories
                        }
                        $route->param($matchName, $matches[$matchName]); // feed them into the route
                    }
                }
                return true;
            }

        }

        /**
         * Returns the chosen route.
         *
         * @access public
         * @return AbstractRoute
         */
        public function getWhichRoute(){
            return $this->whichRoute;
        }

        /**
         * Sets the chosen route given an instance of a route.
         *
         * @access public
         * @param AbstractRoute $whichRoute
         * @return $this
         */
        public function setWhichRoute(AbstractRoute $whichRoute){
            $this->whichRoute = $whichRoute;
            return $this;
        }

        /**
         * Determines which route supports the current HTTP method and matches the current URI.
         *
         * Based extensively on (basically an exact copy of) the wonderful Slim Framework's Router (http://www.slimframework.com/) released under the MIT Public License (http://www.slimframework.com/license)
         * Slim Framework is copyright of Josh Lockhart (https://github.com/codeguy)
         *
         * @access public
         * @return mixed
         */
        public function whichRoute(){
            $routes = $this->routeCollection->getRoutes();
            foreach($routes as $route){
                if(
                    (
                        $route->supportsMethod($this->method) ||
                        $route->supportsMethod('*')
                    ) &&
                    $this->parseRouteAndMatchURI($route)
                ){
                    $this->setWhichRoute($route);
                    return $this->getWhichRoute();
                }
            }
            return false; // fallback
        }

        /**
         * Executes the callback of the matching route.
         *
         * @access public
         * @return void
         */
        public function route(){
            $route = $this->whichRoute();
            if(!$route){
                throw new Router404Exception('No matching route object found.');
            }
            $callbackClass = new \ReflectionClass($route->getCallback());
            if($callbackClass->isSubClassOf('\\Nifty\\Router\\RouteCallbackInterface')){
                $callbackClass = $callbackClass->newInstanceArgs($this->arguments);
                $callbackClass->setURLParams($route->params());
                $callbackClass->run();
            }
        }

    }

    /**
     * RouterServiceException class.
     */
    class RouterServiceException extends \Exception {}

    class Router404Exception extends RouterServiceException {}
