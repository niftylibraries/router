<?php

    /**
     * Defines abstract route collection class
     *
     * Used for identifying and constructing route collection classes.
     *
     * @category   Nifty
     * @package    Router
     * @author     Piers Simms <piers@c1h.co.uk>
     * @copyright  2014 Piers Simms
     * @version    1.0
     * @since      1.0
     */

    namespace Nifty\Router\Collections;

    /**
     * Abstract AbstractRouteCollection class.
     *
     * @abstract
     */
    abstract class AbstractRouteCollection {

        /**
         * Array of individual route classes
         *
         * (default value: array())
         *
         * @var array
         * @access protected
         */
        protected $routes = array();

        /**
         * Returns all route classes as an array.
         *
         * @access public
         * @return array
         */
        public function getRoutes(){
            return $this->routes;
        }

    }

    /**
     * RouteCollectionException class.
     */
    class RouteCollectionException extends \Exception {}