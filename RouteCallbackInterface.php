<?php

    /**
     * Interface for route callbacks
     *
     * Used to ensure that the callback can handle parameters provided when routing to it
     *
     * @category   Nifty
     * @package    Router
     * @author     Piers Simms <piers@c1h.co.uk>
     * @copyright  2014 Piers Simms
     * @version    1.0
     * @since      1.0
     */

    namespace Nifty\Router;

    /**
     * RouteCallbackInterface interface.
     */
    interface RouteCallbackInterface {

        /**
         * Setting parameters must be possible.
         *
         * @access public
         * @param array $params (default: array())
         * @return void
         */
        public function setURLParams($params = array());

        /**
         * Execute and start logic for callback.
         *
         * @access public
         * @return void
         */
        public function run();

    }