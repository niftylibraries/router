<?php

    /**
     * Defines abstract route class and it's properties
     *
     * Used for identifying and constructing route classes
     *
     * @category   Nifty
     * @package    Router
     * @author     Piers Simms <piers@c1h.co.uk>
     * @copyright  2014 Piers Simms
     * @version    1.0
     * @since      1.0
     */

    namespace Nifty\Router;

    /**
     * Abstract AbstractRoute class.
     *
     * @abstract
     */
    abstract class AbstractRoute {

        /**
         * Namespace for class to instantiate once routed
         *
         * (default value: '')
         *
         * @var string
         * @access protected
         */
        protected $callbackClass = '';

        /**
         * Human-writable pattern that will be converted to a regular expression
         *
         * (default value: '')
         *
         * @var string
         * @access protected
         */
        protected $pattern = '';

        /**
         * Array of supported HTTP methods
         *
         * (default value: array())
         *
         * @var array
         * @access protected
         */
        protected $methods = array();

        /**
         * Conditions as regular expressions for parameters in the route's pattern
         *
         * (default value: array())
         *
         * @var array
         * @access protected
         */
        protected $conditions = array();

        /**
         * params
         *
         * (default value: array())
         *
         * @var array
         * @access protected
         */
        protected $params = array();

        /**
         * Returns the namespace for the callback.
         *
         * @access public
         * @return string
         */
        public function getCallback(){
            return $this->callbackClass;
        }

        /**
         * Sets the callback's namespace.
         *
         * @access public
         * @param mixed $callback
         * @return $this
         */
        public function setCallback($callback){
            $this->callbackClass = $callback;
            return $this;
        }

        /**
         * Returns the route's matching pattern.
         *
         * @access public
         * @return string
         */
        public function getPattern(){
            return $this->pattern;
        }

        /**
         * Sets the route's matching pattern.
         *
         * @access public
         * @param mixed $pattern
         * @return $this
         */
        public function setPattern($pattern){
            $this->pattern = $pattern;
            return $this;
        }

        /**
         * Returns the route's supported HTTP methods.
         *
         * @access public
         * @return array
         */
        public function getMethods(){
            return $this->methods;
        }

        /**
         * Sets the route's supported HTTP methods.
         *
         * @access public
         * @param array $methods (default: array())
         * @return mixed $this on success, false on failure
         */
        public function setMethods($methods = array()){
            if(is_string($methods)){
                $methods = array_map('trim', explode(',', $methods));
            }
            $methods = array_map('strtoupper', $methods);
            if(is_array($methods)){
                $this->methods = $methods;
                return $this;
            } else {
                return false;
            }
        }

        /**
         * Returns the pattern parameters' conditions.
         *
         * @access public
         * @return array
         */
        public function getConditions(){
            return $this->conditions;
        }

        /**
         * Sets the pattern parameters' conditions.
         *
         * @access public
         * @param array $conditions (default: array())
         * @return $this
         * @throw RouteException if conditions given are not in an array
         */
        public function setConditions($conditions = array()){
            if(!is_array($conditions)){
                throw new RouteException('Conditions given are not in an array.');
            }
            $this->conditions = $conditions;
            return $this;
        }

        /**
         * Sets or unsets a parameter.
         *
         * @access public
         * @param mixed $name
         * @param mixed $value (default: null)
         * @return $this
         */
        public function param($name, $value = null){
            if(!is_null($value)){
                $this->params[$name] = $value;
            } else {
                unset($this->params[$name]);
            }
            return $this;
        }

        /**
         * Returns all parameters.
         *
         * @access public
         * @return array
         */
        public function params(){
            return $this->params;
        }

        /**
         * Checks if the route supports the HTTP method given.
         *
         * @access public
         * @param mixed $method
         * @return bool
         */
        public function supportsMethod($method){
            return in_array($method, $this->getMethods());
        }

    }

    /**
     * RouteException class.
     */
    class RouteException extends \Exception {}